n = input()
print("The next number for the number ", n ," is ", int(n)+1, "." , sep="")
print("The previous number for the number ", n ," is ", int(n)-1, "." ,sep="")
#2й способ
#print(f"The next number for the number {n} is {int(n)+1}.")
#print(f"The previous number for the number {n} is {int(n)-1}.")
