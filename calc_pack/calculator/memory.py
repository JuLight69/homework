import datetime
def memory():
    with open("memory.txt", mode="at+", encoding='utf-8') as memoryf :
        memoryf.seek(0)
        print(memoryf.read())
        today = datetime.datetime.today()
        print(today.strftime("%Y-%m-%d %H:%M"),": view memory", file=memoryf)

def cleanmemory():
    with open("memory.txt", mode="at+", encoding='utf-8') as memoryf :
        memoryf.seek(0)
        memoryf.truncate()
def wrmemory(wstr):
    with open("memory.txt", mode="at+", encoding='utf-8') as memoryf :
        today = datetime.datetime.today()
        print(today.strftime("%Y-%m-%d %H:%M"), wstr , file=memoryf)
