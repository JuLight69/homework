import datetime
def calculation(arg1):
    try:
        a = float(arg1.split(" ")[0])
        operation = arg1.split()[1]
        b = float(arg1.split()[2])
        result = {"+": a + b, "-": a - b,"*": a * b,
                        "/": a / b, "^": a ** b }
        r = result.get(operation)
    except (IndexError):
        res ="ошибка: введены неверные данные"
        print(res)
        return res
    except (ValueError):
        res = "ошибка: введены неверные числа"
        print(res)
        return res
    if r == None:
        res = "ошибка: неверная операция"
        print(res)
        return res
    else:
        print(a, operation, b, "=", r, sep=' ')
        res = str(a) + str(operation) + str(b) + "=" + str(r)
    return res

def funcdef():
    print('''           Доступные команды:
                        exit - завершает текущую сессию вычислений
                        memory - вызывает историю операций
                        clean memory - очистка истории операций
                        Доступные вычисления:
                        -ввести первое число
                        -через знак пробела ввести одну из операций: +, -, *, /, ^
                        -через знак пробела ввести второе втрое число ''')
    return
