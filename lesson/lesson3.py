'''
#строки
string = "Hello,world"
print(string[4],
      string[6])
print(len(string))
lst = [1,2,3]
lst[2] = 4
string1 ='Hello \n world?'
'''
'''
#списки
lst = list("hellomf")
lst[5]="n"
del lst[6]
print(lst)
'''
#кортеж
tpl5 = 1,
tpl6 = 1, False, 45
tpl7 = tuple([1,3])
print(tpl7)
