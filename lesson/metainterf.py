class MetaClass(type):
    _method = ("print", "check")

    def __new__(cls, name, bases, attribute):
        for method in cls._method:
            if method not in attribute:
                for base in bases:
                    if hasattr(base, method):
                        break
                    else:
                        raise TypeError(f"have no {method} method")
        return super().__new__(cls, name, bases, attribute)

class Printer(metaclass=MetaClass):
    def print(self): pass
    def check(self): pass
class APrinter(Printer):
    def print(self): pass
    def check(self): pass
class BPrinter(Printer):
    def print(self): pass
