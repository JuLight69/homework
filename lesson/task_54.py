class Stack:
    def  __init__(self):
        self._list = []

    def size(self):
        print(len(self._list))

    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        print(self._list.pop())

    def back(self):
        print(self._list[len(self._list)-1])

    def clear(self):
        self._list = []
        print("ok")

    def exit(self):
        print("bye")
        quit()

stack = Stack()
while(1):
    req = [x for x in input().split(" ")]
    func = getattr(stack, req[0])
    func(*req[1:])
    #if req[0] == "push":
    #    exec("stack.{}({})".format(req[0],int(req[1])))
    #else:
    #    exec("stack.{}()".format(req[0]))
