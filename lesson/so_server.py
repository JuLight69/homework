import socket
from threading import Thread

sock = socket.socket()
print('$create socket')

sock.bind(("", 2000))
print("$bind")

sock.listen(1)
print("$listen")

def f1():
        while True:
            try :
                data = conn.recv(1024)
                print(f"{addr[0]} : {addr[1]} >> {data.decode()}")

            except ConnectionAbortedError:
                break
        conn.close()

def f2():
        while True:
            try :
                print("Введите собщение >>")
                text = input()
                conn.send(text.encode())

            except ConnectionAbortedError:
                break
        conn.close()

thread1 = Thread(target=f1)
thread2 = Thread(target=f2)
while True:
    conn, addr = sock.accept()
    print(f"$get connection with {addr}")
    thread1.start()
    thread2.start()

sock.close()

'''
while True:
    conn, addr = sock.accept()
    print(f"$get connection with {addr}")
    while True:
        try :
            data = conn.recv(1024)
            print(f"{addr[0]} : {addr[1]} >> {data.decode()}")
            text = input()
            conn.send(text.encode())

        except ConnectionAbortedError:
            break
    conn.close()

'''
