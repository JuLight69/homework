import socket

sock = socket.socket()
print('$ create socket')

while True:
    sock.connect(("192.168.2.13", 2000))
    print("$connect")
    while True:
        try :
            text = input()
            sock.send(text.encode())
            print("$send str")

            data = sock.recv(1024)
            print("get data")
            print(data.decode())
            
        except ConnectionAbortedError:
            break
sock.close()
