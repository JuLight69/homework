class RegularClass:
    pass
class SlotsClass:
    __slots__ = ("foo",)
class ChildSlotsrClass(SlotsClass):
    pass
obj = RegularClass()
print(obj.__dict__)
obj.foo = 5
print(obj.__dict__)
'''
obj1 = SlotsClass()
print(obj1.__dict__)
obj1.foo = 5
print(obj.__dict__)
'''
obj2 = ChildSlotsrClass()
print(obj2.__dict__)
obj2.foo = 5
obj2.bar = 20
print(obj2.__dict__)


#низя
#class A:
#    __slots__ = ("a",)

#class B:
#    __slots__ = ("b",)

#class C(A,B):
#    __slots__ = ()
