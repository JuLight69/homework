from threading import Thread, Lock
from time import sleep
from multiprocessing import Process
import os
class MyProcess(Process):
    def __init__(self, end, timeout, daemon=False,lock=None):
        super().__init__(daemon=daemon)
        self.end = end
        self.timeout = timeout
        self.lock = lock
    def run(self):
        global begin
        while begin < self.end:
            with self.lock:
                print(f" process {os.getpid()}  ", begin)
                begin +=1
            sleep(self.timeout)

begin = 0
if __name__ == '__main__':
    lock = Lock()
    p1 = MyProcess( 20, 1, lock=lock)
    p2 = MyProcess( 30, 0.5, lock=lock)
    print("START")
    p1.start()
    p2.start()
    print("End")
