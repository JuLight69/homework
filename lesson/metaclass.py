'''
name = "NewClass"
parents = ()
atributes = {"a": 1,
                "b": 2,}
NewClass = type(name, parents, atributes)
print(NewClass)

'''
class MetaClass(type):
    def __new__(self, *args):
        print("create by metaclass")
        cls = type(*args)
        cls.__init__(*args)
        return(cls)
    def __init__(self, *args):
        print("initialize by metaclass")

class Class(metaclass=MetaClass):
    pass
