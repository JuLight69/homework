class MyZ(ArithmeticError):
    def __init__(self,number):
        super(ArithmeticError,self).__init__("number pumber")
        self.number = number

from calca.ops import div
try:
    print(div(1000,0))
except ZeroDevisionError as e:
    raise MyZ(1000)
