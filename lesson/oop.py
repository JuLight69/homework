'''
class Animal:
    pass
a1 = Animal()
a1 = Animal()
print(a1.__class__)
'''
'''
#incaps
class animal:
    weight = 200
    color = 'red'
    print('hello')
a1 = animal()
a2 = animal()
print(a1.weight)
a1.color = 'green'
a2.weight = 400
a1.size = 21
print(a1.size)
del a1.size
animal.weight = 50
animal.color = 'blue'
'''
'''
#methods
class animal:
    weight = 200
    color = 'red'
    def __init__(self,name):
        self.name = name

    def func(self):
        print(f'weight {self.name} : {self.weight}')

    def __del__(self):
        print(f"deleted {self.name}")
a1 = animal('murka')
a1.func()
del a1
'''
'''
#inheritance
class Animal:
    def __init__(self,name):
        self.name = name

    def sound(self):
        print("any sound")

    def eat(self,weight):
        print(f'{self.name} eat: {weight}')

    def __del__(self):
        print(f"deleted {self.name}")

class Cow(Animal):
    #def sound(self):
        #print("Moooo")
        pass
murka =Cow('Murka')
murka.sound()
class Horse(Animal):
    def run(self,dist):
        print(f"run for {dist}")
    def sound(self):
        print("igogogo")
class Bird(Animal):
    def fly(self,dist):
        print(f"flied for {dist}")
    def sound(self):
        print("chirick")
class Pegas(Bird,Horse):
    def __init__(self,name,mana):
        super().__init__(name)
        self.mana = mana
    def magic(self):
        print(f" {self.name} make magic")
pegas = Pegas('pegas',300)
pegas.sound()
pegas.eat(20)
pegas.run(100)
pegas.fly(300)
pegas.eat(30)
'''

#обработка ошибок
print("1" + 2)
print(100 / 0)
