from time import time
#линейный поиск O(n)
'''
lst = [int(x) for x in input().split(" ")]
need = int(input())
for index, element in enumerate(lst):
    if element == need:
        print(f'found in {index}')
        break
else:
    print('not found')
'''
#бинарный поиск O(n)
lst = [int(x) for x in input().split(" ")]
lst.sort()
need = int(input())
'''
# 1 py O(log2(n))
lst_copy = lst.copy()
iter = 0
while lst_copy:
    iter +=1
    med = len(lst_copy)//2
    if lst_copy[med] < need:
        lst_copy = lst_copy[med + 1:]
    elif lst_copy[med] > need:
        lst_copy = lst_copy[:med]
    else:
        print('found')
        break
else :
    print(f"not found for {iter} iteration")
'''
# 2 algoritm
t1 =time()
left = 0
right = len(lst) - 1
iter = 0
while left <= right:
    iter += 1
    med = (left + right)//2
    if lst[med] < need:
        left = med + 1
    elif lst[med] > need:
        right = med -1
    else:
        t2 = time()
        print(f'found t {t2 - t1}')
        break
else :
    print(f"not found for {iter} iteration t {t2 - t1}")
