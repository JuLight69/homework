class Stack:
    def  __init__(self):
        self._list = []

    def size(self):
        print(len(self._list))

    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        if self._list:
            print(self._list.pop())
        else:
            print("error")
    def back(self):
        if self._list:
            print(self._list[len(self._list)-1])
        else:
            print("error")
    def clear(self):
        self._list = []
        print("ok")

    def exit(self):
        print("bye")
        quit()

stack = Stack()
while(1):
    req = [x for x in input().split(" ")]
    func = getattr(stack, req[0])
    func(*req[1:])
