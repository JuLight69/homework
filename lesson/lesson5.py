'''
res = print("I")
print(res)

def premium(salary, percent = 50):
    result = salary / 100 * percent
    return result

s1 = int(input())
p1 = int(input())

prem = premium(s1, p1)
print(s1 + prem)


def m (a0, *a1, a2 = 4, **a3):
    print(a0)
    print(a1)
    print(a2)
    print(a3)
    print("-"*10)

m(1)
m(1,2)
m(1,2,3)
m(1,2,3,a3 = 50)
m(1,2,3, a2 =80,a3 = 100)


def fibonacci(index):
    if index in (0, 1):
        return index
    return fibonacci(index - 2) + fibonacci(index - 1)
print(fibonacci(6))
'''

def funct1():
    def funct2():
        z = 3
    z = 2
    func2()
func1()
