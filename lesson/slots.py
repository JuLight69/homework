'''
class MyClass:
    __slots__ = ( "_value")
    def set(self, value):
        self._value = value
    def get(self):
        self._value
my_obj = MyClass()
my_obj.set(20)
print(my_obj.get())

#my_obj.p = 900
del my_obj._value

print(dir(my_obj))
'''
import timeit
class Foo:
    __slots__ = ("foo",)
class Bar:
    pass

def get_set_delete(obj):
    obj.foo = 'test'
    obj.foo
    del obj.foo

foo = min(timeit.repeat(lambda: get_set_delete(Foo())))

bar = min(timeit.repeat(lambda: get_set_delete(Bar())))
print(foo, bar)
