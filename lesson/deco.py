def arguments(function):
    def wrappper(*args,**kwargs):
        print(f"args {function.__name__}: {args}")
        print(f"kwargs {function.__name__}: {kwargs}")
        return function(*args,**kwargs)
    return wrappper
def result(prompt):
    def decorator(function):
        def wrappper(*args,**kwargs):
            result = function(*args,**kwargs)
            print(f"{prompt} result {function.__name__}: {result}")
            return result
        return wrappper
    return decorator
@result("log")
@arguments
def div(a, b):
    return a/b
#div = arguments(div)
div(100,b=10)
