class OneParent(type):
    def __new__(cls, name, bases, atribute):
        if len(bases) > 1:
            raise TypeError("Multiple base classes")

        return super().__new__(cls, name, bases, atribute)

class Base(metaclass=OneParent): pass
print('Base created')
class A(Base): pass
print('A created')
class B(Base): pass
print('B created')
class C(A, B): pass
print('C created')
