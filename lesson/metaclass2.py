
class EntityMeCl(type):
    def __new__(cls, name, bases, atribute):
        new_cls = super().__new__(cls, name, bases, atribute)
        new_cls.__del__ = lambda self: print("i am deleted")
        return new_cls

class Entity(metaclass=EntityMeCl):
    def __init__(self, name):
        self.name = name
        print(f'{name} created')


a = Entity("a")
b = Entity("b")
