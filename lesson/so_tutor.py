import socket

sock = socket.socket()
print('create socket')
sock.bind("", 2000)
print("bind")
sock.listen(1)
print("listen")
conn, addr = sock.accept()
print(f"get connection with {addr}")
data = conn.recv(1024)
print("get data")
conn.send(b"Hey,bro")
print("send str")

conn.close()
sock.close()
