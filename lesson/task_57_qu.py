class Queue:
    def  __init__(self):
        self._list = []

    def size(self):
        print(len(self._list))

    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        print(self._list.pop(0))

    def front(self):
        print(self._list[0])

    def clear(self):
        self._list = []
        print("ok")

    def exit(self):
        print("bye")
        quit()

q = Queue()
while(1):
    req = [x for x in input().split(" ")]
    func = getattr(q, req[0])
    func(*req[1:])
