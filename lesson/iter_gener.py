'''
lst = [1, 2, 3, 4, 5]
for element in lst:
    print (element, end=", ")

print("__iter__ in lst: " , hasattr(lst, "__iter__"))
it = iter(lst)

for element in lst:
    try:
        print(next(it), end=", ")
        print(next(it), end=", ")
    except Exception as e:
        print("it ended", e)
    print(element, end=", ")
'''
'''
#iter
class SimpleIterator:
    def __init__(self, count):
        self.now = 0
        self.count = count

    def __next__(self):
        self.now +=1
        if self.now > self.count:
            raise StopIteration
        return self.now

class SimpleIterable:
    def __init__(self, count):
        self.count = count
    def __iter__(self):
        return SimpleIterator(self.count)


it = SimpleIterable(3)
for el in it :
    for el2 in it:
        print(el, el2)
'''
#gener
def xrange(start, end, step):
    while start < end:
        start += step
        yield start

    return start
gen = xrange(1, 3, 0.5)
print('gen: ', gen)
print(next(gen))

for el in xrange(1, 3, 0.5):
    print(el, end=' ')
print()

def fobonacci():
    prev, now = 0 ,1
    while True:
        yield now
        now, prev = prev + now, now
