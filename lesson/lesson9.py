from threading import Thread, Lock
from time import sleep
from multiprocessing import Process
import os
'''
def func1(name,start,end,timeout):

    while start < end:
        print (f"Thread {name} :  ", start)
        start += 1
        sleep(timeout)

thread1 = Thread(target=func1, args=("f1",0, 20, 1),daemon=True)
thread2 = Thread(target=func1, args=("f2",0, 30, 0.5))
print("START")
start = 0
thread1.start()
thread2.start()
func1("main",0, 10, 1)
#thread2.join()
#thread1.join()
print("T1 end")
print("End")

#потоки через классы
class MyThread(Thread):
    def __init__(self, name, end, timeout, daemon=False,lock=None):
        super().__init__(daemon=daemon)
        self.name = name
        self.end = end
        self.timeout = timeout
        self.lock = lock
    def run(self):
        global begin
        while begin < self.end:
            with self.lock:
                print(f" thread {self.name}  ", begin)
                begin +=1
            sleep(self.timeout)

lock = Lock()
begin = 0
thread1 = MyThread("1", 20, 1, lock=lock)
thread2 = MyThread("2", 30, 0.5, lock=lock)
print("START")
thread1.start()
thread2.start()
print("End")

def func(name,start,end,timeout):

    while start < end:
        print (f"{os.getpid()} :  ", start)
        start += 1
        sleep(timeout)

if __name__ == '__main__':
    process1 = Process(target=func, args=("f1",0, 20, 1))
    process2 = Process(target=func, args=("f2",0, 30, 0.5))
    print("START")

    process1.start()
    process2.start()
    func(f"main {os.getpid()}",0, 10, 1)
#thread2.join()
#thread1.join()
    print("End")

def decorator(fu):
    def wrapper(*args,**kwargs):
        print('a',args)
        print('k',kwargs)
        return fu(*args,*kwargs)
    return wrapper
def func(a, b):
    return a**b
func = decorator(func)
print(func(4,2))

'''
import asyncio

async def func(name,start,end,timeout):
    while start < end:
        print (f"corountine{name} :  ", start)
        start += 1
        await asyncio.sleep(timeout)

loop = asyncio.get_event_loop()
future = asyncio.gather(
    func("1",2,23,2),
    func("2",3,22,21))
loop.run_until_complete(future)
loop.close()
