from time import time
class Queue:
    def  __init__(self):
        self._list = []

    def size(self):
        print(len(self._list))

    def push(self, value):
        self._list.append(value)
        print("ok")

    def pop(self):
        if self._list:
            print(self._list.pop(0))
        else:
            print("error")

    def front(self):
        if self._list:
            print(self._list[0])
        else:
            print("error")
    def clear(self):
        self._list = []
        print("ok")

    def exit(self):
        print("bye")
        quit()

q = Queue()
while(1):
    t1 = time()
    req = [x for x in input().split(" ")]
    func = getattr(q, req[0])
    func(*req[1:])
    print(time() - t1, " c")
