def rectangle(n, m):
    for i in range(n):
        if (i==0) or (i==n-1):
            print("A" * m)
        else:
            print("A", " " * (m - 4), "A")
    return
print("Веедите число строк и столбцов через знак пробела:")
try:
    str = (input()).split(" ")
    rectangle(int(str[1]), int(str[0]))
except (IndexError, ValueError):
    print("Введены неверные данные")
