import pygame
import random
from os import path
from pygame.locals import *

img_dir = path.join(path.dirname(__file__), 'myimg')
snd_dir = path.join(path.dirname(__file__), 'snd')

WIDTH = 1000
HEIGHT = 650
BAR_LENGTH = 100
BAR_HEIGHT = 10
FPS = 60
POWERUP_TIME = 5000
SCORE = 0.5

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

# Создаем игру и окно
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("*magic battle*")
clock = pygame.time.Clock()
font_name = pygame.font.match_font('Arial')

def draw_text(surf, text, size, x, y, color):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

def newmob():
    m = Mob()
    all_sprites.add(m)
    mobs.add(m)


def draw_shield_bar(surf, x, y, pct):
    if pct < 0:
        pct = 0
    fill = (pct / 100) * BAR_LENGTH
    outline_rect = pygame.Rect(x, y, BAR_LENGTH, BAR_HEIGHT)
    fill_rect = pygame.Rect(x, y, fill, BAR_HEIGHT)
    pygame.draw.rect(surf, BLUE, fill_rect)
    pygame.draw.rect(surf, WHITE, outline_rect, 2)


def draw_lives(surf, x, y, lives, img):
    for i in range(lives):
        img = pygame.transform.scale(img, (30, 50))
        img_rect = img.get_rect()
        img_rect.x = x + 30 * i
        img_rect.y = y
        surf.blit(img, img_rect)
    img_recth = pygame.Rect( WIDTH / 5, 10, 10, 20)
    screen.blit(hum, img_recth)
    img_rects = pygame.Rect( WIDTH * 0.76, 10, 10, 20)
    screen.blit(stena, img_rects)


def show_go_screen():
    screen.blit(backgrounds, backgrounds_rect)
    draw_text(screen, "*magic battle*", 64, WIDTH / 2, HEIGHT * 2 / 10, WHITE)
    draw_text(screen, "ARROW keys move, SPACE to fire", 22,
              WIDTH / 2, HEIGHT * 4 / 10, WHITE)
    draw_text(screen, "If you want to die:", 22,
              WIDTH / 2, HEIGHT * 5 / 10, RED)
    draw_text(screen, "- let the monster eat the inhabitants", 22,
              WIDTH / 2, HEIGHT * 6 / 10, WHITE)
    draw_text(screen, "- let the monster destroy the castle wall", 22,
              WIDTH / 2, HEIGHT * 7 / 10, WHITE)
    draw_text(screen, "or just let them eat YOU", 22,
              WIDTH / 2, HEIGHT * 8 / 10, WHITE)
    draw_text(screen, "Press a key to try to save somebody", 18, WIDTH / 2, HEIGHT * 9 / 10, RED)
    with open('maxscore.txt', 'r') as f:
        max = f.read()
    draw_text(screen, f"max score {max}", 18, WIDTH / 2, HEIGHT * 1 / 10, WHITE)
    pygame.display.flip()
    waiting = True
    while waiting:
        clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.KEYUP:
                waiting = False


class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(player_img, (40, 70))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.radius = 20
        self.rect.centerx = WIDTH / 2
        self.rect.centery = HEIGHT - 40
        self.speedx = 0
        self.speedx = 0
        self.shield = 100
        self.shoot_delay = 500
        self.last_shot = pygame.time.get_ticks()
        self.lives = 3
        self.hidden = False
        self.hide_timer = pygame.time.get_ticks()
        self.power = 1
        self.power_time = pygame.time.get_ticks()

    def update(self):
        if self.power >= 2 and pygame.time.get_ticks() - self.power_time > POWERUP_TIME:
            self.power -= 1
            self.power_time = pygame.time.get_ticks()

        if self.hidden and pygame.time.get_ticks() - self.hide_timer > 1000:
            self.hidden = False
            self.rect.centerx = WIDTH / 2
            self.rect.bottom = HEIGHT - 10

        self.speedx = 0
        self.speedy = 0
        keystate = pygame.key.get_pressed()
        if keystate[pygame.K_LEFT]:
            self.speedx = -9
        if keystate[pygame.K_RIGHT]:
            self.speedx = 9
        if keystate[pygame.K_UP]:
            self.speedy = -9
        if keystate[pygame.K_DOWN]:
            self.speedy = 9

        if keystate[pygame.K_SPACE]:
            self.shoot()

        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.right > WIDTH:
            self.rect.right = WIDTH
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.top <= 0:
            self.rect.top = 0
        if self.rect.bottom >= HEIGHT:
            self.rect.bottom = HEIGHT

    def powerup(self):
        self.power += 1
        self.power_time = pygame.time.get_ticks()

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            if self.power == 1:
                bullet = Bullet(self.rect.centerx, self.rect.top, -10, bullet_img)
                all_sprites.add(bullet)
                bullets.add(bullet)
                shoot_sound.play()
            if self.power >= 2:
                bullet1 = Bullet(self.rect.left, self.rect.centery, -10,bullet_img)
                bullet2 = Bullet(self.rect.right, self.rect.centery, -10,bullet_img)
                all_sprites.add(bullet1)
                all_sprites.add(bullet2)
                bullets.add(bullet1)
                bullets.add(bullet2)
                shoot_sound.play()

    def hide(self):
        # временно скрыть игрока
        self.hidden = True
        self.hide_timer = pygame.time.get_ticks()
        self.rect.center = (WIDTH / 2, HEIGHT + 200)

class Wall(pygame.sprite.Sprite):
    def __init__(self, screen):
        pygame.sprite.Sprite.__init__(self)
        self.image_orig = pygame.image.load(path.join(img_dir, "wall.png")).convert()
        self.image = pygame.transform.scale(self.image_orig, (WIDTH, 30))
        self.rect = pygame.Rect(0, HEIGHT - 30 , WIDTH, 30)
        self.health = 100
        self.health_wall = 1000
        self.screen = screen
    def draw(self):
        self.screen.blit(self.image, self.rect)
        #pygame.draw.rect(screen,BLACK,self.rect)

class FlyMobs(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(flymob_img, (30, 50))
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.radius = 20
        self.rect.centerx = WIDTH / 2
        self.rect.bottom = 50
        self.speedy = random.randrange(1, 2)
        self.speedx = random.randrange(-2, 2)
        self.shoot_delay = 1000
        self.last_shot = pygame.time.get_ticks()


    def update(self):
        self.rect.x += self.speedx
        self.rect.y += self.speedy

        if self.rect.bottom >= HEIGHT / 2 or self.rect.top <= 0:
            self.speedy = -self.speedy
        if self.rect.left <= 0 or self.rect.right >= WIDTH:
            self.speedx = -self.speedx

        self.shoot()


    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            bullet = Bullet(self.rect.centerx, self.rect.bottom + 20, 10, bullet2_img)
            all_sprites.add(bullet)
            mob_bullets.add(bullet)
            shoot_sound.play()


class Mob(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image_orig = random.choice(monster_images)
        self.image_orig.set_colorkey(BLACK)
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * .85 / 2)
        self.rect.x = random.randrange(WIDTH - self.rect.width)
        self.rect.y = random.randrange(-150, -100)
        self.speedy = random.randrange(1, 4)
        self.speedx = random.randrange(-2, 2)

        self.last_update = pygame.time.get_ticks()

    def update(self):
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top > HEIGHT + 10 or self.rect.right <= 0 or self.rect.left >= WIDTH:
            self.rect.x = random.randrange(WIDTH - self.rect.width)
            self.rect.y = random.randrange(-100, -40)
            self.speedy = random.randrange(1, 4)


class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y, speedy,bullt):
        pygame.sprite.Sprite.__init__(self)
        self.image = bullt
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedy = speedy


    def update(self):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()


class Pow(pygame.sprite.Sprite):
    def __init__(self, center):
        pygame.sprite.Sprite.__init__(self)
        self.type = random.choice(['shield', 'gun'])
        self.image = powerup_images
        #self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.speedy = 2

    def update(self):
        self.rect.y += self.speedy
        if self.rect.top > HEIGHT:
            self.kill()


class Explosion(pygame.sprite.Sprite):
    def __init__(self, center, size):
        pygame.sprite.Sprite.__init__(self)
        self.size = size
        self.image = explosion_anim[self.size][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = pygame.time.get_ticks()
        self.frame_rate = 50

    def update(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > self.frame_rate:
            self.last_update = now
            self.frame += 1
            if self.frame == len(explosion_anim[self.size]):
                self.kill()
            else:
                center = self.rect.center
                self.image = explosion_anim[self.size][self.frame]
                self.rect = self.image.get_rect()
                self.rect.center = center


# Загрузка игровой графики
backgrounds = pygame.image.load(path.join(img_dir, "start.jpg")).convert()
backgrounds = pygame.transform.scale(backgrounds, (WIDTH, HEIGHT))
backgrounds_rect = backgrounds.get_rect()

background = pygame.image.load(path.join(img_dir, "snow.png")).convert()
background = pygame.transform.scale(background, (WIDTH, HEIGHT))
background_rect = background.get_rect()

player_img = pygame.image.load(path.join(img_dir, "Mage0.png")).convert()
player_mini_img = pygame.transform.scale(player_img, (50, 30))
player_mini_img.set_colorkey(BLACK)

playerli_img = pygame.image.load(path.join(img_dir, "Mage.png")).convert()
playerli_mini_img = pygame.transform.scale(playerli_img, (50, 30))
playerli_mini_img.set_colorkey(BLACK)

hum = pygame.image.load(path.join(img_dir, "hum.png")).convert()
hum = pygame.transform.scale(hum, (20, 30))
hum.set_colorkey(BLACK)


stena = pygame.image.load(path.join(img_dir, "str.png")).convert()
stena = pygame.transform.scale(stena, (20, 30))
stena.set_colorkey(BLACK)

flymob_img = pygame.image.load(path.join(img_dir, "m2.png")).convert()
flymob_img = pygame.transform.scale(flymob_img, (50, 30))

bullet_img = pygame.image.load(path.join(img_dir, "laserRed16.png")).convert()

bullet2_img = pygame.image.load(path.join(img_dir, "laserBlue16.png")).convert()

monster_images = []
monster_list = ['m1.png', 'monster4.png']
for img in monster_list:
    monster_images.append(pygame.image.load(path.join(img_dir, img)).convert())

explosion_anim = {}
explosion_anim['lg'] = []
explosion_anim['sm'] = []
explosion_anim['player'] = []
for i in range(9):
    filename = 'regularExplosion0{}.png'.format(i)
    img = pygame.image.load(path.join(img_dir, filename)).convert()
    img.set_colorkey(BLACK)
    img_lg = pygame.transform.scale(img, (75, 75))
    explosion_anim['lg'].append(img_lg)
    img_sm = pygame.transform.scale(img, (32, 32))
    explosion_anim['sm'].append(img_sm)
    filename = 'sonicExplosion0{}.png'.format(i)
    img = pygame.image.load(path.join(img_dir, filename)).convert()
    img.set_colorkey(BLACK)
    explosion_anim['player'].append(img)

powerup_images = pygame.image.load(path.join(img_dir, 'zel1.png')).convert()
powerup_images = pygame.transform.scale(powerup_images, (50, 50))
powerup_images.set_colorkey(BLACK)


# Загрузка мелодий игры
shoot_sound = pygame.mixer.Sound(path.join(snd_dir, '016.wav'))
shield_sound = pygame.mixer.Sound(path.join(snd_dir, '016.wav'))
power_sound = pygame.mixer.Sound(path.join(snd_dir, '016.wav'))
scream = pygame.mixer.Sound(path.join(snd_dir, 'scream.wav'))
expl_sounds = []
for snd in ['016.wav', '016.wav']:
    expl_sounds.append(pygame.mixer.Sound(path.join(snd_dir, snd)))

create_score = 500
wall = Wall(screen)

# Цикл игры
game_over = True
running = True
while running:
    if game_over:
        show_go_screen()

        game_over = False
        all_sprites = pygame.sprite.Group()
        mobs = pygame.sprite.Group()
        bullets = pygame.sprite.Group()
        mob_bullets = pygame.sprite.Group()
        powerups = pygame.sprite.Group()
        player = Player()
        all_sprites.add(player)

        for i in range(4):
            newmob()
        score = 0

    score += SCORE

    if score > create_score:
        for i in range(2):
            flymob = FlyMobs()
            all_sprites.add(flymob)
            mobs.add(flymob)
        create_score += 500

    # Держим цикл на правильной скорости
    clock.tick(FPS)
    # Ввод процесса (события)
    for event in pygame.event.get():
        # проверка для закрытия окна
        if event.type == pygame.QUIT:
            running = False
        if event.type == KEYUP:
            if event.key == K_p:
                pause = True
                while pause:

                    for event in pygame.event.get():
                        if event.type == KEYUP:
                            if event.key == K_p:
                                pause = False

    # Обновление
    all_sprites.update()

    # не попала ли пуля в мобa
    hits = pygame.sprite.groupcollide(mobs, bullets, True, True)
    for hit in hits:
        #score += 50 - hit.radius
        random.choice(expl_sounds).play()
        expl = Explosion(hit.rect.center, 'lg')
        all_sprites.add(expl)
        if random.random() > 0.9:
            pow = Pow(hit.rect.center)
            all_sprites.add(pow)
            powerups.add(pow)
        newmob()


    #  Проверка, не ударил ли моб игрока
    hits = pygame.sprite.spritecollide(player, mobs, True, pygame.sprite.collide_circle)
    for hit in hits:
        player.shield -= hit.radius * 2
        expl = Explosion(hit.rect.center, 'sm')
        all_sprites.add(expl)
        newmob()
        if player.shield <= 0:
            death_explosion = Explosion(player.rect.center, 'player')
            all_sprites.add(death_explosion)
            player.hide()
            player.lives -= 1
            player.shield = 100

    # Проверка попал ли выстрел в игрока
    hits = pygame.sprite.spritecollide(player, mob_bullets, True, pygame.sprite.collide_circle)
    for hit in hits:
        player.shield -= 30
        #expl = Explosion(hit.rect.center, 'sm')
        #all_sprites.add(expl)
        if player.shield <= 0:
            death_explosion = Explosion(player.rect.center, 'player')
            all_sprites.add(death_explosion)
            player.hide()
            player.lives -= 1
            player.shield = 100

    # Проверка попал ли выстрел в стену
    hits = pygame.sprite.spritecollide(wall, mob_bullets, True)
    for hit in hits:
        wall.health_wall -= 25
        expl = Explosion(hit.rect.center, 'sm')
        all_sprites.add(expl)
        if wall.health_wall <= 0:
            wall.health_wall = 1000
            game_over = True
    # Проверка столкновений игрока и улучшения
    hits = pygame.sprite.spritecollide(player, powerups, True)
    for hit in hits:
        if hit.type == 'shield':
            player.shield += random.randrange(10, 30)
            if player.shield >= 100:
                player.shield = 100
        if hit.type == 'gun':
            player.powerup()
            power_sound.play()

    # мобы прорвались за стену, милорд
    hits = pygame.sprite.spritecollide(wall, mobs, True)
    for hit in hits:
        scream.play()
        wall.health -= 2
        newmob()
        print(wall.health)
        if wall.health <= 0:
            wall.health = 100
            game_over = True

    # Если игрок умер, игра окончена
    if player.lives == 0 and not death_explosion.alive():
        with open('maxscore.txt', 'r') as f:
            max = f.read()
        if int(score) > int(max):
            with open('maxscore.txt', 'w') as f:
                f.write(str(int(score)))
        create_score = 500
        game_over = True

    # Рендеринг
    screen.fill(BLACK)
    screen.blit(background, background_rect)
    wall.draw()
    all_sprites.draw(screen)



    draw_text(screen, str(int(score)), 18, WIDTH / 2, 10, BLACK)
    draw_text(screen, str(int(wall.health)), 18, WIDTH / 4, 10, BLACK)
    draw_text(screen, str(int(wall.health_wall)), 18, WIDTH * 0.8, 10, BLACK)
    draw_shield_bar(screen, 5, 5, player.shield)
    draw_lives(screen, WIDTH - 100, 5, player.lives,
               playerli_mini_img)


    pygame.display.flip()

pygame.quit()
