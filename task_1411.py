n = int(input())
lst = list(map(int, input().split()))
i = 0
for index1 in range(1, n + 1):
    for index2 in range(0, n - index1):
        if lst[index2] > lst[index2 + 1]:
            lst[index2], lst[index2 + 1] = lst[index2 + 1], lst[index2]
            i +=1
print(i)
