import pymysql
from pymysql.cursors import DictCursor
import datetime
from tabulate import tabulate
conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    password='',
    db='new_schema',
    charset='utf8mb4',
    cursorclass=DictCursor
)

print("bd connection")

with conn.cursor() as cursor:
    date = datetime.datetime.now()
    sql_drop_prod = "DROP TABLE products"
    sql_create_prod = '''CREATE TABLE products
                        (id INT AUTO_INCREMENT,
                        name VARCHAR(50) NOT NULL unique,
                        price FLOAT,
                        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY(id))
                    '''
    sql_create_stat = '''CREATE TABLE statistics
                       (id INT AUTO_INCREMENT PRIMARY KEY,
                       product_id INT,
                       created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                       FOREIGN KEY (product_id) REFERENCES products(id))
                    '''
    sql_drop_stat = "DROP TABLE statistics"
    # командa запроса (Execute Query)
    cursor.execute(sql_drop_stat)
    cursor.execute(sql_drop_prod)
    cursor.execute(sql_create_prod)

    cursor.execute('''INSERT INTO products (name, price) VALUES ('Vodka', 59.90)''')
    cursor.execute('''INSERT INTO products (name, price) VALUES ('Voda', 19.90)''')
    cursor.execute('''INSERT INTO products (name, price) VALUES ('Hleb', 10.40)''')

    cursor.execute(sql_create_stat)

    cursor.execute('''INSERT INTO statistics (product_id) VALUES (1)''')
    cursor.execute('''INSERT INTO statistics (product_id) VALUES (2)''')
    cursor.execute('''INSERT INTO statistics (product_id) VALUES (1)''')
    cursor.execute('''INSERT INTO statistics (product_id) VALUES (1)''')

    cursor.execute("SELECT * FROM products  ")
    results = cursor.fetchall()
    print(tabulate(results, headers='keys', tablefmt="grid"))

    cursor.execute("SELECT * FROM statistics  ")
    results = cursor.fetchall()
    print(tabulate(results, headers='keys', tablefmt="grid"))
