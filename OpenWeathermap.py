import requests
city = input()
response = requests.get( f"http://api.openweathermap.org/data/2.5/weather?q={city}&APPID=5d1c5370369029f2d3d9274729db73b2")
wether_info = response.json()
print("Temperature:", round(wether_info['main']['temp'] - 273), " Celcius")
print("Wind speed: ", wether_info['wind']['speed'], " m/s")
w_deg = wether_info['wind']['deg']

degree =int((w_deg/22.5)+.5)
carr=["N","NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"]
print(f"Direction of the wind: {carr[(degree % 16)]}" )
