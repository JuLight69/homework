
def is_year_leap(years):
    if not (years % 4):
        result = "YES"
    elif (years % 100 != 0) and (years % 400 == 0):
        result = "YES"
    else:
        result = "NO"
    return result
a = int(input())
print(is_year_leap(a))
