import datetime
def calculation(a, b, operatinon):
    result = {"+": a + b, "-": a - b,"*": a * b,
                "/": a / b, "^": a ** b }
    return result.get(operatinon)
exit = 1
with open("C:/Users/User/Desktop/memory.txt", mode="at+", encoding='utf-8') as memoryf :
    print('''           Доступные команды:
                        exit - завершает текущую сессию вычислений
                        memory - вызывает историю операций
                        clean memory - очистка истории операций
                        Доступные вычисления:
                        -ввести первое число
                        -через знак пробела ввести одну из операций: +, -, *, /, ^
                        -через знак пробела ввести второе втрое число ''')
    while(exit):
        print('''Введите новую команду:''')
        arg1 = input()
        if (arg1 == "exit"):
            exit = 0
        elif arg1 == "memory":
            memoryf.seek(0)
            print(memoryf.read())
            today = datetime.datetime.today()
            print(today.strftime("%Y-%m-%d %H:%M"),": view memory", file=memoryf)
        elif arg1 == "clean memory":
            memoryf.seek(0)
            memoryf.truncate()
        else:
            try:
                arg = arg1.split(" ")[0]
                operation = arg1.split()[1]
                arg2 = arg1.split()[2]
                res = calculation(float(arg), float(arg2),operation)
            except (IndexError):
                print("ошибка: введены неверные данные")
                continue
            except (ValueError):
                print("ошибка: введены неверные числа")
                continue
            if res == None:
                print("ошибка: неверная операция")
            else:
                print(arg, operation, arg2, "=", res, sep=' ')
                today = datetime.datetime.today()
                print(today.strftime("%Y-%m-%d %H:%M"),":", arg, operation, arg2, "=",
                        res, sep=' ', file=memoryf)
